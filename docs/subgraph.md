# Subgraph ERC20-token-balances

This is to setup a subgraph that indexes and queries ERC20 token balances for Ethereum accounts.

## Software and Versions used
`@graphprotocol/graph-cli/0.50.1 linux-x64 node-v20.2.0`

```bash
npm install -g npm@9.6.7
npm install -g @graphprotocol/graph-cli@0.50.1
```

## Initialize

```
zarathustra@Sils-Maria ~/P/rust_task [1]> graph init --studio
✔ Protocol · ethereum
✔ Subgraph slug · ERC20-token-balances
✔ Directory to create the subgraph in · ERC20-token-balances
✔ Ethereum network · mainnet
✔ Contract address · 0x0000000000000000000000000000000000000000
✖ Failed to fetch ABI from Etherscan: ABI not found, try loading it from a local file
✖ Failed to fetch Start Block: Failed to fetch contract creation transaction hash
  
✔ ABI file (path) · IERC20.json
✔ Start Block · 0
✔ Contract Name · ERC20
✔ Index contract events as entities (Y/n) · true
  Generate subgraph
  Write subgraph to directory
✔ Create subgraph scaffold
✔ Initialize networks config
✔ Initialize subgraph repository
✔ Install dependencies with npm install
✔ Generate ABI and schema types with npm run codegen
Add another contract? (y/n): 
Subgraph ERC20-token-balances created in ERC20-token-balances
```

## Copy over files

From the root directory of this repository:

```bash
cp -rf subgraph/. ERC20-token-balances/
```

## Codegen

```bash
cd ERC20-token-balances/
graph auth --studio <YOUR_DEPLOY_KEY>
graph codegen &&  graph build
graph deploy --studio erc20-token-balances
```


## Qeury

Simple query for balances of the first account.

```graphql
{
  accounts(where: { id: "<YOUR_ADDRESS>" }, block: { number: 1060152 }) {
    id
    balances {
      id
      token {
        id
        name
        symbol
        decimals
      }
      amount
    }
  }
}
```

May try it [here](https://thegraph.com/studio/subgraph/erc20-token-balances/playground).  Some test inputs:

```bash
id: "0x0000000000000000000000000000000000000000"
number: 1060152
```

```bash
id: "0x0000000000000000000000000000000000000000"
number: 1196056
```

or run it directly from CLI

```bash
curl -X POST -H "Content-Type: application/json" --data '{ "query": "{ accounts(where: { id: \"0x0000000000000000000000000000000000000000\" }, block: { number: 1196056 }) { id balances { id token { id name symbol decimals } amount } } }" }' https://api.studio.thegraph.com/query/47505/erc20-token-balances/v0.0.2
```

expected output:
```bash
{"data":{"accounts":[{"id":"0x0000000000000000000000000000000000000000","balances":[{"id":"0x94f27b5141e17dd8816242d752c7be8e6764bd22-0x0000000000000000000000000000000000000000","token":{"id":"0x94f27b5141e17dd8816242d752c7be8e6764bd22","name":"ETHER","symbol":"ETH","decimals":"8"},"amount":"5000000000000000"},{"id":"0xe1e22ddbff3d6ecc8e9a4ac5af8f9192b91c28dd-0x0000000000000000000000000000000000000000","token":{"id":"0xe1e22ddbff3d6ecc8e9a4ac5af8f9192b91c28dd","name":"Ubersoft Class B Common Shares","symbol":"","decimals":"0"},"amount":"1000000000"}]}]}}
```