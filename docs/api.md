# API Documentation

The API provides endpoints to retrieve historic token balances for a specific address.

## Base URL

The base URL for the API is http://localhost:8080.

## Endpoints

### Get Address Balance

Retrieves the historic token balances for a given address at a specific timestamp.
- Endpoint: /get_address_balance
- Method: GET

Parameters
Name	Type	Description
address	String	The Ethereum address for which to retrieve balances.
timestamp	Integer	The timestamp for which to retrieve balances.

Example request

```bash
curl -X GET "http://localhost:8080/get_address_balance?address=0x0000000000000000000000000000000000000000&timestamp=3000000"
```

example response
```json
{
  "0x0000000000000000000000000000000000000000": {
    "0x5a16268e63c3099f9b0125992aaa1a3a2ac739a1": "1000",
    "0xedbaf3c5100302dcdda53269322f3730b1f0416d": "989831000",
    "0x8fff600f5c5f0bb03f345fd60f09a3537845de0a": "1",
    "0xd8912c10681d8b21fd3742244f44658dba12264e": "20.6",
    "0xd6da8d25e313a39fbd42f4b6eb641b2ed775815d": "5",
    "0xbb9bc244d798123fde783fcc1c72d3bb8c189413": "10000000000000001",
    "0xa2e958fb88d1c1d07cc694f79852ce61e4ddc06d": "100000",
    "0x6ccf0b344a0484ae49c157c6a3e41e56fbe715ed": "200",
    "0xaec2e87e0a235266d9c5adc9deb4b2e29b54d009": "1",
    "0x888666ca69e0f178ded6d75b5726cee99a87d698": "1",
    "0x9863cdc84dccd6615c0fa94771b83e3e42a2bdb5": "1",
    "0x4a41659df69d663d000764d3b235908e5937c6b2": "5000000000000000000",
    "0xe1e22ddbff3d6ecc8e9a4ac5af8f9192b91c28dd": "1000000000",
    "0x0312deed361db866e9e3a04f35f833783a32e16f": "999100",
    "0xa74476443119a942de498590fe1f2454d7d4ac0d": "617498.391490810000150119",
    "0x14f37b574242d366558db61f3335289a5035c506": "50",
    "0x4df812f6064def1e5e029f1ca858777cc98d2d81": "63998.5",
    "0x5c40ef6f527f4fba68368774e6130ce6515123f2": "70952380952380952381",
    "0x8360c91b02e60e4c1db4e2d298e2f2c441d1aca4": "400",
    "0x48c80f1f4d53d5951e5d5438b54cba84f29f32a5": "6034.658095763129202794",
    "0x790262b3fce1962a09762176f00de765f6ae916e": "2",
    "0x8acc3dff6bfad5378cee6c75b4ce81a186571c14": "500",
    "0x7eab3cc7234f66508b713ed3338edeb3c894d14c": "10",
    "0x94f27b5141e17dd8816242d752c7be8e6764bd22": "50000000",
    "0xb6223a359f7eac788761be8b1026493d09ea4bf1": "13700",
    "0xe0b7927c4af23765cb51314a0e0521a9645f0e2a": "456123456789",
    "0x28f9eece184ddf04f93c6bd8977924f7f62308bd": "998999",
    "0x45e42d659d9f9466cd5df622506033145a9b89bc": "24325.653",
    "0x1e797ce986c3cff4472f7d38d5c4aba55dfefe40": "1000",
    "0x569348f5204f0491a137c0b8bd5b0ab72c5a659c": "5",
    "0xbef795cd40aff527f0b992e68da40249338a4317": "0.365",
    "0xaf30d2a7e90d7dc361c8c4585e9bb7d2f6f15bc7": "10000",
    "0xa1e83bce056237b54248c2e629eb064067270cf0": "1",
    "0x0fc9b7ba475040a0ebb432987cff6ff22d4ae950": "1000",
    "0x5c543e7ae0a1104f78406c340e9c64fd9fce5170": "3000000000000000000000",
    "0x8ce332263632c671d98b6c7aaacf504db4850651": "100"
  }
}
```

## Error Handling

The API follows standard HTTP status codes for error responses. Here are some common status codes you may encounter:

- 400 Bad Request: Indicates that the request parameters are invalid or missing.
- 404 Not Found: Indicates that the requested resource was not found.
- 500 Internal Server Error: Indicates an unexpected error occurred on the server.


## Docker

build the image

```bash
sudo docker build -t erc20-token-balances .
```

ensure you have the following declared in your `.env` file

```bash
GRAPH_DEPLOY_KEY=<YOUR_GRAPH_DEPLOY_KEY>
```

run the image to start the localhost API

```bash
sudo docker run -p 8080:8080 --env-file=./.env -v "$(pwd)/data:/app/data" erc20-token-balances
```

and query it from another terminal

```bash
curl -X GET "http://localhost:8080/get_address_balance?address=0x0000000000000000000000000000000000000000&timestamp=3940991"
```
