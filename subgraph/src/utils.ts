import { ERC20 } from "../generated/ERC20/ERC20"
import { Account, Token, } from "../generated/schema"
import { BigDecimal, ethereum, Address } from "@graphprotocol/graph-ts";

export function fetchTokenDetails(event: ethereum.Event): Token | null {

  let token = Token.load(event.address.toHex());
  if (!token) {

    token = new Token(event.address.toHex());

    token.name = "N/A"
    token.symbol = "N/A"
    token.decimals = BigDecimal.fromString("0")

    let erc20 = ERC20.bind(event.address);

    let tokenName = erc20.try_name();
    if (!tokenName.reverted) {
      token.name = tokenName.value;
    }

    let tokenSymbol = erc20.try_symbol();
    if (!tokenSymbol.reverted) {
      token.symbol = tokenSymbol.value;
    }

    let tokenDecimal = erc20.try_decimals();
    if (!tokenDecimal.reverted) {
      token.decimals = BigDecimal.fromString(tokenDecimal.value.toString());
    }

    token.save();
  }
  return token;
}

export function fetchAccount(address: string): Account | null {

  let account = Account.load(address);
  if (!account) {
    account = new Account(address);
    account.save();
  }
  return account;
}

export function fetchBalance(tokenAddress: Address, accountAddress: Address): BigDecimal {
  let erc20 = ERC20.bind(tokenAddress); //bind token
  let amount = BigDecimal.fromString("0")

  let tokenBalance = erc20.try_balanceOf(accountAddress);
  if (!tokenBalance.reverted) {
    amount = BigDecimal.fromString(tokenBalance.value.toString());
  }
  return amount
}
