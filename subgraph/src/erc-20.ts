
import { Transfer } from "../generated/ERC20/ERC20"
import { TokenBalance, TokenBalanceSnapshot } from "../generated/schema"
import {
  fetchTokenDetails,
  fetchAccount,
  fetchBalance
} from "./utils"
import { BigDecimal, BigInt, ethereum, Address } from "@graphprotocol/graph-ts";

export function handleTransfer(event: Transfer): void {
  let token = fetchTokenDetails(event);
  if (!token) {
    return
  }

  let fromAddress = event.params.from.toHex();
  let toAddress = event.params.to.toHex();

  let fromAccount = fetchAccount(fromAddress);
  let toAccount = fetchAccount(toAddress);

  if (!fromAccount || !toAccount) {
    return;
  }

  // Store historical balances for 'from' account
  storeHistoricalBalance(fromAccount.id, token.id, event.block.number);

  let fromTokenBalance = TokenBalance.load(token.id + "-" + fromAccount.id);
  if (!fromTokenBalance) {
    fromTokenBalance = new TokenBalance(token.id + "-" + fromAccount.id);
    fromTokenBalance.token = token.id;
    fromTokenBalance.account = fromAccount.id;
  }

  fromTokenBalance.amount = fetchBalance(event.address, event.params.from)

  if (fromTokenBalance.amount != BigDecimal.fromString("0")) {
    fromTokenBalance.save();
  }

  // Store historical balances for 'to' account
  storeHistoricalBalance(toAccount.id, token.id, event.block.number);

  let toTokenBalance = TokenBalance.load(token.id + "-" + toAccount.id);
  if (!toTokenBalance) {
    toTokenBalance = new TokenBalance(token.id + "-" + toAccount.id);
    toTokenBalance.token = token.id;
    toTokenBalance.account = toAccount.id;
  }
  toTokenBalance.amount = fetchBalance(event.address, event.params.to)
  if (toTokenBalance.amount != BigDecimal.fromString("0")) {
    toTokenBalance.save();
  }
}

function storeHistoricalBalance(accountId: string, tokenId: string, blockNumber: BigInt): void {
  let balanceSnapshot = TokenBalanceSnapshot.load(`${tokenId}-${accountId}-${blockNumber.toString()}`);
  if (!balanceSnapshot) {
    balanceSnapshot = new TokenBalanceSnapshot(`${tokenId}-${accountId}-${blockNumber.toString()}`);
    balanceSnapshot.token = tokenId;
    balanceSnapshot.account = accountId;
    balanceSnapshot.blockNumber = blockNumber;
  }

  let tokenBalance = TokenBalance.load(`${tokenId}-${accountId}`);
  if (tokenBalance) {
    balanceSnapshot.amount = tokenBalance.amount;
  } else {
    balanceSnapshot.amount = BigDecimal.fromString("0");
  }

  balanceSnapshot.save();
}