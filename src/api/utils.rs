use std::collections::HashMap;
use std::fs::File;
use std::io::Write;

pub fn export_balances_to_json(
    balance_data: HashMap<String, HashMap<String, String>>,
    file_path: &str,
) {
    let json_string =
        serde_json::to_string_pretty(&balance_data).expect("Failed to serialize to JSON");

    let mut file = File::create(file_path).expect("Failed to create file");

    file.write_all(json_string.as_bytes())
        .expect("Failed to write to file");
}
