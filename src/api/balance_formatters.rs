use ethereum_types::U256;
use std::collections::HashMap;

use crate::api::models::{Account, Balance, BalanceInfo};

fn format_token_balance(balance: Balance) -> String {
    let decimals = balance
        .token
        .decimals
        .parse::<u8>()
        .expect("Decimals field must be uint8 in ERC20");
    let token_balance =
        U256::from_dec_str(&balance.amount).expect("Balance field must be uint256 in ERC20");
    let divisor = U256::from(10).pow(U256::from(decimals));
    let integer_part = token_balance / divisor;
    let decimal_part = token_balance % divisor;
    format!("{}.{}", integer_part, decimal_part)
        .trim_end_matches("0")
        .trim_end_matches('.')
        .to_owned()
}

fn format_account_balances(account: Account) -> (String, HashMap<String, String>) {
    let balances = account
        .balances
        .into_iter()
        .map(|balance| {
            let balance_id = balance
                .id
                .strip_suffix(&format!("-{}", &account.id))
                .unwrap_or(&balance.id)
                .to_owned();
            (balance_id, format_token_balance(balance))
        })
        .collect();

    (account.id, balances)
}

pub fn format_balance_info(balance_info: BalanceInfo) -> HashMap<String, HashMap<String, String>> {
    balance_info
        .accounts
        .into_iter()
        .map(format_account_balances)
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::api::models::{Balance, Token};

    #[test]
    fn test_format_token_balance_non_zero_decimal_part() {
        // Test case 1: Non-zero decimal part
        let token1 = Token {
            id: "0x0000000000000000000000000000000000000000".to_string(),
            name: "Ethereum".to_string(),
            symbol: "ETH".to_string(),
            decimals: "18".to_string(),
        };
        let balance1 = Balance {
            id: "0x1".to_owned(),
            token: token1,
            amount: "1234560000000000000000".to_owned(),
        };

        let formatted_balance = format_token_balance(balance1);

        assert_eq!(formatted_balance, "1234.56");
    }

    #[test]
    fn test_format_token_balance_zero_decimal_part() {
        // Test case 2: Zero decimal part
        let token2 = Token {
            id: "0x0000000000000000000000000000000000000001".to_string(),
            name: "Token 2".to_string(),
            symbol: "TKN2".to_string(),
            decimals: "0".to_string(),
        };
        let balance2 = Balance {
            id: "0x2".to_owned(),
            token: token2,
            amount: "5000".to_owned(),
        };

        let formatted_balance = format_token_balance(balance2);

        assert_eq!(formatted_balance, "5000");
    }

    #[test]
    fn test_format_token_balance_excess_decimal_zeroes() {
        // Test case 3: Excess decimal zeroes
        let token3 = Token {
            id: "0x0000000000000000000000000000000000000002".to_string(),
            name: "Token 3".to_string(),
            symbol: "TKN3".to_string(),
            decimals: "2".to_string(),
        };
        let balance3 = Balance {
            id: "0x3".to_owned(),
            token: token3,
            amount: "123000".to_owned(),
        };

        let formatted_balance = format_token_balance(balance3);

        assert_eq!(formatted_balance, "1230");
    }
}
