use serde_json::json;

pub fn generate_query(address: &str, block_number: u64) -> String {
    let query = include_str!("../../subgraph/accounts_query.graphql");
    let variables = json!({
        "address": address,
        "blockNumber": block_number,
    });

    let query_json = json!({
        "query": query,
        "variables": variables,
    });

    serde_json::to_string(&query_json).expect("Failed to serialize query")
}
