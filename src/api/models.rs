use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct ApiResponse<T> {
    pub data: T,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct BalanceInfo {
    pub accounts: Vec<Account>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Account {
    pub id: String,
    pub balances: Vec<Balance>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Balance {
    pub id: String,
    pub token: Token,
    pub amount: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Token {
    pub id: String,
    pub name: String,
    pub symbol: String,
    pub decimals: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct BalanceQuery {
    pub address: String,
    pub timestamp: Option<u64>,
}
