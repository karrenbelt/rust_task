use rweb::reject::Reject;
use serde_json::Error as JsonError;

#[derive(Debug)]
pub struct JsonDeserializationError {
    pub error: JsonError,
    pub response_body: String,
}

impl Reject for JsonDeserializationError {}

impl From<(JsonError, String)> for JsonDeserializationError {
    fn from((error, response_body): (JsonError, String)) -> Self {
        JsonDeserializationError {
            error,
            response_body,
        }
    }
}

#[derive(Debug)]
pub enum ApiError {
    SubgraphRequestFailed,
    InvalidResponseFormat,
}

impl Reject for ApiError {}
