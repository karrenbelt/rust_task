use crate::api::balance_formatters::format_balance_info;
use crate::api::errors::{ApiError, JsonDeserializationError};
use crate::api::models::{ApiResponse, BalanceInfo, BalanceQuery};
use crate::api::query::generate_query;
use crate::api::utils::export_balances_to_json;

use reqwest::Client;
use rweb::reject::custom;
use serde_json::from_str;

/// Handles the balance request for a given address and block number using a subgraph endpoint.
///
/// # Arguments
///
/// * `subgraph_endpoint` - The URL of the subgraph endpoint.
/// * `query` - The balance query containing the address and optional timestamp (block number).
/// * `default_block_number` - The default block number to use if the timestamp is not provided.
///
/// # Returns
///
/// Returns a JSON response containing the exported balance data or an error if the subgraph request fails.
///
/// # Example
///
/// ```rust
/// use crate::api::models::BalanceQuery;
///
/// let subgraph_endpoint = "https://api.studio.thegraph.com/query/47505/erc20-token-balances/v0.0.2";
/// let query = BalanceQuery { address: "0x1234567890abcdef", timestamp: Some(1000000) };
/// let default_block_number = 1196056;
///
/// let response = balance_handler(subgraph_endpoint, query, default_block_number).await;
/// ```
pub async fn balance_handler(
    subgraph_endpoint: &str,
    query: BalanceQuery,
    default_block_number: u64,
) -> Result<impl rweb::Reply, rweb::Rejection> {
    let address = &query.address;
    let block_number = query.timestamp.unwrap_or(default_block_number);

    let query = generate_query(&address, block_number);
    let response = Client::new()
        .post(subgraph_endpoint)
        .header("Content-Type", "application/json")
        .body(query)
        .send()
        .await
        .map_err(|_| custom(ApiError::SubgraphRequestFailed))?;

    if response.status().is_success() {
        let response_body = response
            .text()
            .await
            .map_err(|_| custom(ApiError::InvalidResponseFormat))?;

        let api_response: ApiResponse<BalanceInfo> = from_str(&response_body)
            .map_err(|err| JsonDeserializationError::from((err, response_body)).into())
            .map_err(|err| custom::<JsonDeserializationError>(err))?;

        let balance_info = api_response.data;
        let balance_data = format_balance_info(balance_info);

        let file_path = format!("data/{}_{}.json", address, block_number);

        export_balances_to_json(balance_data, &file_path);

        Ok(rweb::reply::json(&format!(
            "JSON file exported: {}",
            file_path
        )))
    } else {
        Err(custom(ApiError::SubgraphRequestFailed))
    }
}
