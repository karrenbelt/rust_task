mod api;

use crate::api::models::BalanceQuery;
use api::handlers;
use env_logger;
use log::LevelFilter;
use rweb::get;
use rweb::query;
use rweb::Filter;

#[tokio::main]
async fn main() {
    env_logger::builder().filter_level(LevelFilter::Info).init();
    log::info!("Server started successfully.");

    let subgraph_endpoint =
        "https://api.studio.thegraph.com/query/47505/erc20-token-balances/v0.0.2";
    // TODO: change default to latest once subgraph is synced
    let default_block_number = 1196056;

    let api_routes = get()
        .and(rweb::path("get_address_balance"))
        .and(query::<api::models::BalanceQuery>())
        .and_then(move |query: BalanceQuery| {
            log::info!("Received balance request: {:?}", query);
            handlers::balance_handler(subgraph_endpoint, query, default_block_number)
        })
        .boxed();

    rweb::serve(api_routes).run(([0, 0, 0, 0], 8080)).await;
}
