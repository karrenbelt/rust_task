# Rust Task
Implementation should be made on a fork of this repo. 

## Introduction
The task consists in building an application that exports historic token balances in a JSON file through a localhost API. The API exposes an endpoint:    
`/get_address_balance?address=<ADDRESS>&timestamp=<TIMESTAMP>`  
This returns e file for a mapping of `token_address` to `token_balance` adjusted for decimals:
```json
{
    "0x...1": 123.456,
    "0x...2": 0.123456,
    "0x...3": 12345.6,
}
```
Only mainnet balances are required, in case other networks are added the format of the query and response JSON should reflect that.  
The response should include all ERC20 tokens in the wallet at the given timestamp.
Timestamp can be assumed to be after latest node pruning, use `1684164385` for testing. 

## Datasources 
The allowed datasources to be used for this are: blockchain RPCs (preffered, url below), Subgraphs, Dune Analytics, other similar event indexing services. Any external code used for preprocessing in these datasources, like SQL or GraphQL queries, the Subgraph code etc. should be included in this repo.  
Not allowed are services like Metis, Alchemy API, Etherscan, or any other API that gives the response directly. Basically not allowed to just forward the request to another service.

## Crates
Usage of any crate is allowed.  
For any chain interaction [ethers](https://crates.io/crates/ethers) is **mandatory**.  
For API [rweb](https://crates.io/crates/rweb) is prefered. 

## Notes
The difficulty lies in being able to get balances for an unknown set of tokens.  
System does not necessarily need to be super scalable, but it's a plus if it is.  
Tokens with re-base (like stETH) increase in balance without any transfers, accounting correctly for these is a plus.   
Being able to query the latest timestamp and getting up-to-date reponse is a plus, the more accurate the better (latest block is best).

## Alchemy RPCs
`https://eth-mainnet.g.alchemy.com/v2/taAAmWozCzHTtPSRLSXC1TTF0ChUMd_L`