# Stage 1: Build stage
FROM rust:1.69.0 as builder

# Set the working directory inside the container
WORKDIR /app

# Copy the Cargo.toml and Cargo.lock files to the container
COPY Cargo.toml Cargo.lock ./

# Build the project without the actual source code
RUN mkdir src && \
    echo "fn main() {println!(\"Dummy main.rs\")}" > src/main.rs && \
    cargo build --release && \
    rm -f target/release/deps/erc20_token_balances*

# Copy the entire project directory to the container
COPY . .

# Build the project
RUN cargo build --release

# Stage 2: Final stage
FROM rust:1.69.0

# Set the working directory inside the container
WORKDIR /app

# Copy the built binary from the builder stage
COPY --from=builder /app/target/release/erc20-token-balances ./

# Expose the port your application listens on (if applicable)
EXPOSE 8080

# Set the entry point command to run your application
CMD ["./erc20-token-balances"]